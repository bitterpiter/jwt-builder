<?php

use JWT\Parts\Payload;
use JWT\Token;
use Lcobucci\JWT\Builder as JWTBuilder;
use Lcobucci\JWT\Token as JWTToken;

/**
 * Class Builder
 */
class Builder
{
    const REGISTERED_CLAIMS = ['audience', 'uniqueId', 'issuedAt', 'notBefore', 'expiration', 'subject', 'issuer'];

    /**
     * @var Token
     */
    private $token;

    /**
     * @var JWTToken
     */
    private $jwtToken;

    /**
     * Builder constructor.
     * @param Token $token
     */
    public function __construct(Token $token)
    {
        $this->token = $token;
    }

    /**
     * @throws \BadMethodCallException
     */
    public function build()
    {
        $jwtBuilder = new JWTBuilder();
        $this->setPayloadData($jwtBuilder);
        $this->signBuilder($jwtBuilder);

        $this->jwtToken = $jwtBuilder->getToken();
    }

    /**
     * @return string
     * @throws UnbuiltTokenException
     */
    public function get()
    {
        if (!$this->jwtToken instanceof JWTToken) {
            throw new UnbuiltTokenException();
        }

        return $this->jwtToken->__toString();
    }

    /**
     * @param JWTBuilder $jwtBuilder
     * @throws \BadMethodCallException
     */
    private function setPayloadData(JWTBuilder $jwtBuilder)
    {
        $payload = $this->token->getPayload();
        $this->setRegisteredClaims($jwtBuilder, $payload);
        $this->setAdditionalClaims($jwtBuilder, $payload);
    }

    /**
     * @param JWTBuilder $jwtBuilder
     * @param Payload $payload
     */
    private function setRegisteredClaims(JWTBuilder $jwtBuilder, Payload $payload)
    {
        foreach (self::REGISTERED_CLAIMS as $claimName) {
            $claimValue = $payload->{'get' . ucfirst($claimName)}();
            if ($claimValue !== null) {
                $jwtBuilder->{'set' . ucfirst($claimName)}();
            }
        }
    }

    /**
     * @param JWTBuilder $jwtBuilder
     * @param Payload $payload
     * @throws \BadMethodCallException
     */
    private function setAdditionalClaims(JWTBuilder $jwtBuilder, Payload $payload)
    {
        $data = $payload->getData();

        if (empty($data)) {
            return;
        }

        foreach ($data as $key => $value) {
            $jwtBuilder->set($key, $value);
        }
    }

    /**
     * @param JWTBuilder $jwtBuilder
     */
    private function signBuilder(JWTBuilder $jwtBuilder)
    {
        $signature = $this->token->getSignature();
        $jwtBuilder->sign($signature->getSigner(), $signature->getKey());
    }
}
